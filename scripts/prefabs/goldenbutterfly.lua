local assets=
{
	Asset("ANIM", "anim/butterfly_basic.zip"),
}
    
local prefabs =
{
    "butterflywings",
    "butter",
    "flower",
}

local function TrackInSpawner(inst)
    local ground = GetWorld()
    if ground and ground.components.goldenbutterflyspawner then
        ground.components.goldenbutterflyspawner:StartTracking(inst)
    end
end

local function StopTrackingInSpawner(inst)
    local ground = GetWorld()
    if ground and ground.components.goldenbutterflyspawner then
        ground.components.goldenbutterflyspawner:StopTracking(inst)
    end
end



local function OnWorked(inst, worker)
    if worker.components.inventory then
	inst.components.health:SetVal(0)

	--local loot = SpawnPrefab("goldnugget")
        --worker.components.inventory:GiveItem(loot, nil, Vector3(TheSim:GetScreenPos(inst.Transform:GetWorldPosition())))
        worker.SoundEmitter:PlaySound("dontstarve/common/butterfly_trap")
    end
end

local function fadeout(inst)
    inst.components.fader:StopAll()
    inst.AnimState:PlayAnimation("swarm_pst")
    inst.components.fader:Fade(0.5, 0, .75+math.random()*1, function(v) inst.Light:SetIntensity(v) end, function() inst:AddTag("NOCLICK") inst.Light:Enable(false) end)
end


local function fn(Sim)
	local inst = CreateEntity()
	
    inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
    inst.Transform:SetTwoFaced()

    
    
    ----------
    
    inst:AddTag("goldenbutterfly")
    inst:AddTag("insect")
    inst:AddTag("smallcreature")
   
    MakeCharacterPhysics(inst, 1, .25)
    inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    
    
    inst.AnimState:SetBuild("butterfly_basic")
    inst.AnimState:SetBank("butterfly")
    inst.AnimState:PlayAnimation("idle")
    inst.AnimState:SetRayTestOnBB(true);
    
    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor:SetSlowMultiplier( 1 )
    inst:SetStateGraph("SGbutterfly")
     
    ------------------
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(1)


	inst:AddComponent("pollinator")
    ------------------
    
    inst:AddComponent("combat")
    inst.components.combat.hiteffectsymbol = "butterfly_body"
    ------------------

    inst:AddComponent("knownlocations")

    ------------------
    MakeSmallBurnableCharacter(inst, "butterfly_body")
    MakeTinyFreezableCharacter(inst, "butterfly_body")
    
    inst:AddComponent("inspectable")
    ------------------
    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetLoot({"goldnugget"})
    ------------------
    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.NET)
    inst.components.workable:SetWorkLeft(1)
    inst.components.workable:SetOnFinishCallback(OnWorked)
    ------------------

    inst:ListenForEvent( "clocktick", function()
	       if inst.components.highlight then
    else
	    inst:AddComponent("highlight")
    end
    	inst.components.highlight:SetAddColour(Vector3(255/255, 255/255, 255/255) )
end, GetWorld())


    ------------------

    inst:AddComponent("fader")

    local light = inst.entity:AddLight()
    light:SetFalloff(1)
    light:SetIntensity(0.5)
    light:SetRadius(1)
    light:Enable(true)
    light:SetColour(180/255, 195/255, 150/255)

    inst.components.fader:Fade(0, 0.5 , 3+math.random()*2, function(v) inst.Light:SetIntensity(v) end, function() end)
    ------------------

    local brain = require "brains/goldenbutterflybrain"
    inst:SetBrain(brain)

    inst:ListenForEvent("onremove", StopTrackingInSpawner)

    return inst
end

return Prefab( "forest/common/goldenbutterfly", fn, assets, prefabs)

