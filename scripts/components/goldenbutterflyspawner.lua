local GoldenButterflySpawner = Class(function(self, inst)
    self.inst = inst
    self.inst:StartUpdatingComponent(self)
    self.butterflys = {}
    self.timetospawn = 10 + math.random(400)
    self.butterflycap = 1
    self.numbutterflys = 0
    self.prefab = "goldenbutterfly"
end)

function GoldenButterflySpawner:GetSpawnPoint(player)
    local radius = 6+math.random()*6
    local closestFlower = FindEntity(player, radius, nil, {"flower"})
    return closestFlower
end

function GoldenButterflySpawner:StartTracking(inst)
    inst.persists = false
    if not inst.components.homeseeker then
	    inst:AddComponent("homeseeker")
	end

	self.butterflys[inst] = function()
	    if self.butterflys[inst] then
	        inst:Remove()
	    end
	end

	self.inst:ListenForEvent("entitysleep", self.butterflys[inst], inst)
	
	self.numbutterflys = self.numbutterflys + 1
end

function GoldenButterflySpawner:StopTracking(inst)
    inst.persists = true
	inst:RemoveComponent("homeseeker")
	if self.butterflys[inst] then
		self.inst:RemoveEventCallback("entitysleep", self.butterflys[inst], inst)
		self.butterflys[inst] = nil
		self.numbutterflys = self.numbutterflys - 1
	end
end


function GoldenButterflySpawner:OnUpdate( dt )
	local maincharacter = GetPlayer()
    local day = GetClock():IsDawn()
    if maincharacter then
	    
		if self.timetospawn > 0 then
			self.timetospawn = self.timetospawn - dt
		end
	    
		if maincharacter and day and GetWorld().components.seasonmanager:IsSummer() and self.prefab then
			if self.timetospawn <= 0 then
				local spawnFlower = self:GetSpawnPoint(maincharacter)
				if spawnFlower and self.numbutterflys < self.butterflycap then
					local butterfly = SpawnPrefab(self.prefab)
					local spawn_point = Vector3(spawnFlower.Transform:GetWorldPosition() )
					butterfly.Physics:Teleport(spawn_point.x,spawn_point.y,spawn_point.z)
					butterfly.components.pollinator:Pollinate(spawnFlower)
					self:StartTracking(butterfly)
					butterfly.components.homeseeker:SetHome(spawnFlower)
				end
				self.timetospawn = 10 + math.random(400)
			end
		end
	end
    
end


function GoldenButterflySpawner:GetDebugString()
	return "Next spawn: "..tostring(self.timetospawn)
end

return GoldenButterflySpawner
