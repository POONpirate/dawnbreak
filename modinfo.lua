name = "Dawnbreak"
description = "Adds a period of Dawn after Night. Dusk is halved in time."
author = "Heavenfall"
version = "11.1.4.1"
forumthread = "11689"
api_version = 6
icon_atlas = "Heavenfall.xml"
icon = "Heavenfall.tex"

dont_starve_compatible = true
reign_of_giants_compatible = true
shipwrecked_compatible = true

configuration_options =
{
    {
        name = "sleeptilldawn",
		label= "Wake up at dawn instead of morning.",
        options =
        {
            {description = "Enabled", data = true},
            {description = "Disabled", data = false},
        },
        default = true,
    },
    {
        name = "invertrounding",
		label= "Invert Dawn/Dusk rounding.",
        options =
        {
            {description = "Enabled", data = true},
            {description = "Disabled", data = false},
        },
        default = false,
    }
}