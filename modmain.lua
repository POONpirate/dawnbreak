-- Thanks WrathOf for showing me how! -- 


-- clock.lua modifications
function HF_dawnbreak(inst)
	inst.IsDawn = function(self)
		if (self.phase == "dawn") then
			return true
		else
			return false
		end
	end
if GLOBAL.SaveGameIndex:GetCurrentMode() == "survival" or GLOBAL.SaveGameIndex:GetCurrentMode() == "adventure" or GLOBAL.SaveGameIndex:GetCurrentMode() == "shipwrecked" then
	--print("Running HF_dawnbreak")

	-- dawn should be slightly brighter than dusk
	local dawnColour = GLOBAL.Point(150/255, 150/255, 150/255)

	-- alter Clock:SetSegs so that it halves global DuskSegs and sets up global DawnSegs
	inst.SetSegs = function (self, day, dusk, night)
		local norm_time = inst:GetNormEraTime()
		
		self.daysegs = day
		self.nightsegs = night
		self.dusksegs = math.ceil(dusk / 2)
		self.dawnsegs = math.floor(dusk / 2)
		
		if GetModConfigData("invertrounding") then
			self.dusksegs = math.floor(dusk / 2)
			self.dawnsegs = math.ceil(dusk / 2)
		end
		
		if self.phase == "day" then
			self.totalEraTime = self.daysegs*TUNING.SEG_TIME
		elseif self.phase == "dusk" then
			self.totalEraTime = self.dusksegs*TUNING.SEG_TIME
		elseif self.phase == "dawn" then
			self.totalEraTime = self.dawnsegs*TUNING.SEG_TIME
		else
			self.totalEraTime = self.nightsegs*TUNING.SEG_TIME
		end		
		self:SetNormEraTime(norm_time)
		self.inst:PushEvent("daysegschanged")
	end


	-- alter Clock:NextPhase so that it goes to dawn after night
	inst.NextPhase = function(self,inst)

 
  if self:CurrentPhaseIsAlways() then
        self.numcycles = self.numcycles +1
        self.inst:PushEvent("daycomplete", {day= self.numcycles})

        if self.phase == "day" then
            self:StartDay()
        elseif self.phase == "dusk" then
            self:StartDusk()
        else
            self:StartNight()
        end

        return
    end



		if self.phase == "day" then
			self:StartDusk()
		elseif self.phase == "dusk" then
			self:StartNight()
			--print("Start Dusk")
		elseif self.phase == "night" then
			self:StartDawn()
			--print("Start Dawn")
		else
			self.numcycles = self.numcycles +1
			self.inst:PushEvent("daycomplete", {day= self.numcycles})
			self:StartDay()
    		end
	end

	-- custom function to return dawn time
	inst.GetDawnTime = function(self, inst)
		return self.dawnsegs*TUNING.SEG_TIME
	end

	-- custom function to start dawn
	inst.StartDawn = function(self,instant,fromnightvision)
		if not fromnightvision then
			self.timeLeftInEra = self:GetDawnTime()
			self.totalEraTime = self.timeLeftInEra
			self.phase = "dawn"
			self.inst:PushEvent("dawntime", {day=self.numcycles})
			if self.dawntime == 0 then
				self:NextPhase()
			return
			end
		end
		if self.phase ~= self.previous_phase  or fromnightvision then
			self.previous_phase = self.phase
			self:LerpAmbientColour(self.currentColour, dawnColour, instant and 0 or 6)
			print("colour should be right")
		end
	end

	-- alter Clock:GetNormTime so that it understands where Dawn time begins
	inst.GetNormTime = function(self) 
		local ret = 0
		if self.phase == "day" then
			return (self.daysegs / 16)*self:GetNormEraTime()
    		elseif self.phase == "dusk" then
			return (self.daysegs / 16) + (self.dusksegs / 16)*self:GetNormEraTime()
    		elseif self.phase == "dawn" then
			return (self.daysegs / 16) + (self.dusksegs / 16) + (self.nightsegs / 16) + (self.dawnsegs / 16)*self:GetNormEraTime()
    		else
			return (self.daysegs / 16) + (self.dusksegs / 16) + (self.nightsegs / 16)*self:GetNormEraTime()
    		end
   	end

	-- alter Clock:OnLoad so that Dawn is a recognized phase, otherwise defaults to Day (=save in Dawn, loads in Day)
if GLOBAL.IsDLCEnabled(2) then
	inst.OnLoad = function (self, data)
	self.numcycles = data.numcycles or 0
    
    if data.departure_day ~= nil then
        self:SetDepartureDay(data.departure_day)
    end

    if data.timepast ~= nil then
        self.timepast = data.timepast
    end
    --self.numcycles = self.numcycles + data.timepast

	if data.phase  == "night" then
	    self:StartNight(true)
	elseif data.phase  == "dusk" then
       	self:StartDusk(true)
	elseif data.phase  == "dawn" then
    	self:StartDawn(true)
    else 
		self:StartDay(true)
	end

    if data.nightvision then
        self:StartNightVision(true)
    end
    if data.lightningdisabled ~= nil then 
        self.lightningdisabled = data.lightningdisabled
    end 
	local normeratime = data.normeratime or 0
	self:SetNormEraTime(normeratime)
	end
elseif GLOBAL.IsDLCEnabled(GLOBAL.REIGN_OF_GIANTS) then
	inst.OnLoad = function (self, data)
		self.numcycles = data.numcycles or 0
		if data.phase  == "night" then
		    self:StartNight(true)
		elseif data.phase  == "dusk" then
        	self:StartDusk(true)
		elseif data.phase  == "dawn" then
        	self:StartDawn(true)
    	else 
			self:StartDay(true)
		end
		if data.nightvision then
			self:StartNightVision(true)
		end
		local normeratime = data.normeratime or 0
		self:SetNormEraTime(normeratime)
	end
else
	inst.OnLoad = function (self, data)
		self.numcycles = data.numcycles or 0
		if data.phase  == "night" then
		        self:StartNight(true)
		elseif data.phase  == "dusk" then
        		self:StartDusk(true)
		elseif data.phase  == "dawn" then
        		self:StartDawn(true)
    		else 
			self:StartDay(true)
		end
		local normeratime = data.normeratime or 0
		self:SetNormEraTime(normeratime)
	end
end
	-- overwrite Clock:IsDusk() so that dawn counts as dusk, this function is used by forest.lua prefab and will crash the game unless it is either dusk, night or day if it is relevant for the situation in forest.lua. The function cannot be overwritten easily from /mods/ so as a work-around I am rewriting the function it depends on.
	inst.IsDusk = function(self)
		if (self.phase == "dusk") then
			return true
		elseif (self.phase == "dawn") then
			return true
		else
			return false
		end
	end
if GLOBAL.IsDLCEnabled(GLOBAL.REIGN_OF_GIANTS) or GLOBAL.IsDLCEnabled(2) then
	inst.LongUpdate = function (self, dt)
	self:OnUpdate(dt)
	
	--fix the colour
	self.lerptimeleft = 0
    
	if self:IsDay() then
		self.currentColour = self:IsNightVision() and self.dayNightVisionColour or self.dayColour
	elseif self:IsDusk() then
        self.currentColour = self:IsNightVision() and self.duskNightVisionColour or self.duskColour
	elseif self:IsDawn() then
        self.currentColour = dawnColour
	else
        if self:GetMoonPhase() == "full" then
            self.currentColour = self:IsNightVision() and self.fullMoonNightVisionColour or self.fullMoonColour
        else
		  self.currentColour = self:IsNightVision() and self.nightNightVisionColour or self.nightColour
        end
	end
	
    if GLOBAL.GetWorld():IsCave() then
        self.currentColour = self:IsNightVision() and self.caveNightVisionColour or self.caveColour
    end
    
	local p = GLOBAL.GetSeasonManager() and GLOBAL.GetSeasonManager():GetWeatherLightPercent() or 1
	GLOBAL.TheSim:SetAmbientColour( p*self.currentColour.x, p*self.currentColour.y, p*self.currentColour.z )
	end
else
	inst.LongUpdate = function (self, dt)
	
		self:OnUpdate(dt)
	
		--fix the colour
		self.lerptimeleft = 0
		if self:IsDay() then
			self.currentColour = self.dayColour
		elseif self:IsDusk() then
			self.currentColour = self.duskColour
		elseif self:IsDawn() then
			self.currentColour = dawnColour
		else
			self.currentColour = self.nightColour
		end
	
		local p = GLOBAL.GetSeasonManager() and GLOBAL.GetSeasonManager():GetWeatherLightPercent() or 1
		GLOBAL.TheSim:SetAmbientColour( p*self.currentColour.x, p*self.currentColour.y, p*self.currentColour.z )
	end
end
	if GLOBAL.IsDLCEnabled(GLOBAL.REIGN_OF_GIANTS) or GLOBAL.IsDLCEnabled(2) then
		inst.SetNightVision = function(self, on)
			    if on then
			        self.nightvision = true
			        self:StartNightVision()
			    else
			        self.nightvision = false
			        if self.phase == "day" then
	 		           self:StartDay(true, true)
					elseif self.phase == "dawn" then
	 		           self:StartDawn(true, true)  
					elseif self.phase == "dusk" then
	 		           self:StartDusk(true, true)
			        else
			           self:StartNight(true, true)
			        end
	 		   end
		end
	end
	if GetModConfigData("sleeptilldawn") then
		inst.MakeNextDay = function (self, dt)
			local time_left = TUNING.TOTAL_DAY_TIME * (1 - self:GetNormTime())
			self:LongUpdate(time_left - self:GetDawnTime())
		end
	end
end
end
 
AddComponentPostInit("clock", HF_dawnbreak)

local dustbinvar = GLOBAL.require "widgets/statusdisplays"
-- statusdisplays.lua modifications
--function HF_dawnbreakGamePostInit(self)
	-- overwrite statusdisplays.lua UIClock:RecalcSegs() with a version that allows for time after night
	local tempclock = GLOBAL.require "widgets/uiclock"
	local DAY_COLOUR = GLOBAL.Vector3(254/255,212/255,86/255)
	local DUSK_COLOUR = GLOBAL.Vector3(165/255,91/255,82/255)
	local DAWN_COLOUR = GLOBAL.Vector3(205/255,131/255,122/255)
	local DARKEN_PERCENT = .75
	tempclock.RecalcSegs = function (self) 
		local dark = true
    		for k,seg in pairs(self.segs) do
               		local color = nil
               		local daysegs = GLOBAL.GetClock():GetDaySegs()
        		local dusksegs = GLOBAL.GetClock():GetDuskSegs()
			local nightsegs = GLOBAL.GetClock():GetNightSegs()
			local vanillasegs = daysegs+dusksegs+nightsegs
			seg:Show()
			if k <= daysegs then
				color = DAY_COLOUR 
				seg:SetTint(color.x, color.y, color.z, 1)
			elseif k <= daysegs+dusksegs then
				color = DUSK_COLOUR
				seg:SetTint(color.x, color.y, color.z, 1)
			elseif k >= daysegs+dusksegs and k <= daysegs+dusksegs+nightsegs then
				seg:Hide()
			else
				color = DAWN_COLOUR
				seg:SetTint(color.x, color.y, color.z, 1)
			end
		end

	end
	-- overwrite statusdisplays.lua IceOver:Update(dt) to use a table that includes dawn
	local tempalphavalues = {
		day		= 1.0,
		dusk	= 0.6,
		night	= 0.3,
		dawn = 0.6
	}
	local tempiceover = GLOBAL.require "widgets/iceover"
	tempiceover.Update = function(self, dt)
		local clock = GLOBAL.GetClock()
		if clock ~= nil then
			local lerp_factor = clock:LerpFactor()
			local cur_alpha = tempalphavalues[ clock:GetPrevPhase() ]
			local next_alpha = tempalphavalues[ clock:GetPhase() ]
			local new_alpha = ( 1 - lerp_factor ) * cur_alpha + lerp_factor * next_alpha
		self.img:SetTint( 1, 1, 1, new_alpha )
	end

	local lspeed = dt*2
	self.alpha_min = (1 - lspeed) * self.alpha_min + lspeed *self.alpha_min_target
	self.img:SetAlphaRange(self.alpha_min,1)
	
	if self.alpha_min >= .99 then
		self:Hide()
	else
		self:Show()
	end
			
end

PrefabFiles = {
	"goldenbutterfly",
}

local Recipe = GLOBAL.Recipe
local Ingredient = GLOBAL.Ingredient
local RECIPETABS = GLOBAL.RECIPETABS
local STRINGS = GLOBAL.STRINGS

STRINGS.NAMES.GOLDENBUTTERFLY = "Golden Butterfly"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.GOLDENBUTTERFLY = {}
STRINGS.CHARACTERS.GENERIC.DESCRIBE.GOLDENBUTTERFLY.GENERIC = "A sparkly butterfly!"

-- add goldenbutterflyspawner to forest
function HF_dawnbreak_goldenbutterfly_forestcomp(inst)
	inst:AddComponent("goldenbutterflyspawner")
end

if GLOBAL.IsDLCEnabled(2) then
	AddPrefabPostInit("butterfly_areaspawner", HF_dawnbreak_goldenbutterfly_forestcomp)
else
	AddPrefabPostInit("forest", HF_dawnbreak_goldenbutterfly_forestcomp)
end

--wake everything at dawn by default

local dustbinvar = GLOBAL.require "components/sleeper"

GLOBAL.DefaultWakeTest = function(inst)
    if not inst.components.sleeper.nocturnal then
        return GLOBAL.GetClock().phase ~= "night"
        or (inst.components.combat and inst.components.combat.target)
        or (inst.components.burnable and inst.components.burnable:IsBurning() )
        or (inst.components.freezable and inst.components.freezable:IsFrozen() )
        or (inst.components.teamattacker and inst.components.teamattacker.inteam)
        or (inst.components.health and inst.components.health.takingfiredamage)
    else
        return GLOBAL.GetClock():IsDusk() or GLOBAL.GetClock():IsNight()
        or (inst.components.combat and inst.components.combat.target)
        or (inst.components.burnable and inst.components.burnable:IsBurning() )
        or (inst.components.freezable and inst.components.freezable:IsFrozen() )
        or (inst.components.teamattacker and inst.components.teamattacker.inteam)
        or (inst.components.health and inst.components.health.takingfiredamage)
        
    end
end


